'use strict'
const http = require("http");
const fs = require("fs");
const url  = require('url');
const request = require('request');
const mkpath = require('mkpath'),
const excelbuilder = require("msexcel-builder-colorfix"),
const sortJsonArray = require('sort-json-array'),
const bodyParser = require("body-parser"),
const timediff = require('timediff'),
const moment = require('moment');


http.createServer(function(req,res){	
	var url_parts = require('url').parse(req.url, true);
    var query = url_parts.query;
	var date = query.date;
    var url = req.url;
	
	switch(url){
		case '/getSalesReport':
		downloadSalesReport(req,res);
		break;
		
		case '/getActivityReport':
		downloadActivitySheet(req,res);
		break;
	}
	
}).listen(3050);
console.log("server running.........");


function downloadSalesReport(req,res){
	
   var timestamp = Number(new Date());
   var dateobj= new Date() ;
   var day = dateobj.getDate() ;
   var prvDay = new Date((dateobj.setDate(dateobj.getDate()-2)));
   var lastSecondDate = prvDay.getDate();
   var dateobjs= new Date();
   var lastDay = new Date((dateobjs.setDate(dateobjs.getDate()-1)));
   var prvdayPostFix = dayPostFix(lastSecondDate);
   var lastdayPostFix = dayPostFix(lastDay.getDate());
   var postFix = dayPostFix(day);
   var year = dateobj.getFullYear();
   var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
   var monthname = monthNames[dateobj.getMonth()];
   var lsMonthName = monthNames[prvDay.getMonth()];
   var lmonth = monthNames[lastDay.getMonth()];
   if(lsMonthName==monthname){ var month = '1st-'+prvdayPostFix+' '+monthname; }else { var month = '1st '+monthname+'-'+prvdayPostFix+' '+lsMonthName; }
   mkpath('./Reports/Sales', function (err){
        if (err) throw err;
   	    var workbook = excelbuilder.createWorkbook('./Reports/Sales', 'Sales-Report-'+lastdayPostFix+'-'+lmonth+'.xlsx');
        var options = { method: 'GET',
            url: 'http://192.168.99.76/reports/cer/cerSalesReport.php',
            headers: 
            { 'postman-token': '18cac29b-e66e-a8c9-407d-bfdbc0af0eb9',
              'cache-control': 'no-cache' } };

            request(options, function (error, response, body) {
            if (error) throw new Error(error);
             var data = JSON.parse(body);
			 
			// console.log(data);
        for(var key in data[0]){
			var sheetwiseName = key;
			var hqData = data[0][key];
		    var length = Object.keys(data[0][key]).length;
			
		    var sheet1 = workbook.createSheet(key, 100, 120);

            sheet1.set(1, 1, 'POINT'); 
            sheet1.font(1,1,{bold:true,sz:12}); 
            sheet1.border(1, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(1, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(1, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(1, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(1, 1, 'true');
            sheet1.width(1, 20);
		    sheet1.merge({col:1,row:1},{col:1,row:4});
		    sheet1.align(1, 1, 'center');
		    sheet1.valign(1, 1, 'center');
		
            sheet1.set(2, 1, 'OUTLETS GRADE'); 
            sheet1.font(2,1,{bold:true,sz:12}); 
            sheet1.border(2,1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(3, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(4, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(5, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(6, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.wrap(2, 1, 'true');
            sheet1.width(2, 20);
		    sheet1.merge({col:2,row:1},{col:6,row:2});
		    sheet1.align(2, 1, 'center');
		
		    sheet1.set(7, 1, lastdayPostFix+' '+lmonth); 
            sheet1.font(7,1,{bold:true,sz:12}); 
            sheet1.border(7,1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(8, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(9, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(10, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(11, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(12, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(13, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(14, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(15, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(16, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(17, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(18, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.wrap(7, 1, 'true');
            sheet1.width(7, 20);
		    sheet1.merge({col:7,row:1},{col:18,row:1});
		    sheet1.align(7, 1, 'center');
		    sheet1.fill(7, 1, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		    sheet1.fill(8, 1, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		    sheet1.fill(9, 1, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		    sheet1.fill(10, 1, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		    sheet1.fill(11, 1, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		    sheet1.fill(12, 1, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		    sheet1.fill(13, 1, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(14, 1, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(15, 1, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(16, 1, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(17, 1, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(18, 1, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		
		
		    sheet1.set(19, 1, month); 
            sheet1.font(19,1,{bold:true,sz:12}); 
            sheet1.border(19,1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(20, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(21, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(22, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(23, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(24, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(25, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(26, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(27, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(28, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(29, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(30, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.wrap(19, 1, 'true');
            sheet1.width(19, 20);
		    sheet1.merge({col:19,row:1},{col:30,row:1});
		    sheet1.align(19, 1, 'center');
		    sheet1.fill(19, 1, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		    sheet1.fill(20, 1, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		    sheet1.fill(21, 1, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		    sheet1.fill(22, 1, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		    sheet1.fill(23, 1, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		    sheet1.fill(24, 1, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		    sheet1.fill(25, 1, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(26, 1, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(27, 1, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(28, 1, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(29, 1, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(30, 1, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		
		    sheet1.set(31, 1, 'TOTAL'); 
            sheet1.font(31,1,{bold:true,sz:12}); 
            sheet1.border(31,1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(32, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(33, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(34, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(35, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(36, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(37, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(38, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(39, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(40, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(41, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(42, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.wrap(31, 1, 'true');
            sheet1.width(31, 20);
		    sheet1.merge({col:31,row:1},{col:42,row:1});
		    sheet1.align(31, 1, 'center');
		    sheet1.fill(31, 1, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		    sheet1.fill(32, 1, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		    sheet1.fill(33, 1, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		    sheet1.fill(34, 1, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		    sheet1.fill(35, 1, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		    sheet1.fill(36, 1, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		    sheet1.fill(37, 1, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(38, 1, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(39, 1, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(40, 1, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(41, 1, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(42, 1, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		
		    
		    //previous data sub heading..........
		    sheet1.set(7, 2, 'SALE'); 
            sheet1.font(7,2,{bold:true,sz:12}); 
            sheet1.border(7, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(8, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(9, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(10, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(11, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(12, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(13, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(14, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(15, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(7, 2, 'true');
            sheet1.width(7, 20);
		    sheet1.merge({col:7,row:2},{col:15,row:2});
		    sheet1.align(7, 2, 'center');
		    sheet1.valign(7, 2, 'center');
		    sheet1.fill(7, 2, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		    sheet1.fill(8, 2, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		    sheet1.fill(9, 2, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(10, 2, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(11, 2, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(12, 2, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(13, 2, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(14, 2, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(15, 2, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		
		    
		    sheet1.set(16, 2, 'ESTIMATED RETAILER STOCK'); 
            sheet1.font(16,2,{bold:true,sz:12}); 
            sheet1.border(16, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(17, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(18, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(16, 2, 'true');
            sheet1.width(16, 20);
		    sheet1.merge({col:16,row:2},{col:18,row:2});
		    sheet1.align(16, 2, 'center');
		    sheet1.valign(16, 2, 'center');
		    sheet1.fill(16, 2, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		    sheet1.fill(17, 3, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(18, 3, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		
		    //current data sub heading............
		    sheet1.set(19, 2, 'SALE'); 
            sheet1.font(19,2,{bold:true,sz:12}); 
            sheet1.border(19, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(20, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(21, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(22, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(23, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(24, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(25, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(26, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(27, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(19, 2, 'true');
            sheet1.width(19, 20);
		    sheet1.merge({col:19,row:2},{col:27,row:2});
		    sheet1.align(19, 2, 'center');
		    sheet1.valign(19, 2, 'center');
		    sheet1.fill(19, 2, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		    sheet1.fill(20, 2, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		    sheet1.fill(21, 2, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(22, 2, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(23, 2, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(24, 2, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(25, 2, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(26, 2, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(27, 2, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		
		    
		    sheet1.set(28, 2, 'ESTIMATED RETAILER STOCK'); 
            sheet1.font(28,2,{bold:true,sz:12}); 
            sheet1.border(28, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(29, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(30, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(28, 2, 'true');
            sheet1.width(28, 20);
		    sheet1.merge({col:28,row:2},{col:30,row:2});
		    sheet1.align(28, 2, 'center');
		    sheet1.valign(28, 2, 'center');
		    sheet1.fill(28, 2, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		    sheet1.fill(29, 3, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(30, 3, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		
		    //total data sub heading............
		    sheet1.set(31, 2, 'SALE'); 
            sheet1.font(31,2,{bold:true,sz:12}); 
            sheet1.border(31, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(32, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(33, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(34, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(35, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(36, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(37, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(38, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(39, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(31, 2, 'true');
            sheet1.width(31, 20);
		    sheet1.merge({col:31,row:2},{col:39,row:2});
		    sheet1.align(31, 2, 'center');
		    sheet1.valign(31, 2, 'center');
		    sheet1.fill(31, 2, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		    sheet1.fill(32, 2, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		    sheet1.fill(33, 2, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(34, 2, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(35, 2, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(36, 2, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(37, 2, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(38, 2, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(39, 2, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		
		    
		    sheet1.set(40, 2, 'ESTIMATED RETAILER STOCK'); 
            sheet1.font(40,2,{bold:true,sz:12}); 
            sheet1.border(40, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(41, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(42, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(40, 2, 'true');
            sheet1.width(40, 20);
		    sheet1.merge({col:40,row:2},{col:42,row:2});
		    sheet1.align(40, 2, 'center');
		    sheet1.valign(40, 2, 'center');
		    sheet1.fill(40, 2, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		    sheet1.fill(41, 2, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(42, 2, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		
		    //no of outlets child headings.......
		    sheet1.set(2, 3, 'A'); 
            sheet1.font(2,3,{bold:true,sz:11}); 
            sheet1.border(2, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(2, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(2, 3, 'true');
            sheet1.width(2, 7);
		    sheet1.valign(2, 3, 'center');
		    sheet1.align(2, 3, 'center');
			sheet1.merge({col:2,row:3},{col:2,row:4});
		
		    sheet1.set(3, 3, 'B'); 
            sheet1.font(3,3,{bold:true,sz:11}); 
            sheet1.border(3, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(3, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(3, 3, 'true');
            sheet1.width(3, 7);
		    sheet1.valign(3, 3, 'center');
		    sheet1.align(3, 3, 'center');
			sheet1.merge({col:3,row:3},{col:3,row:4});
	
		    sheet1.set(4, 3, 'C'); 
            sheet1.font(4,3,{bold:true,sz:11}); 
            sheet1.border(4, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(4, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(4, 3, 'true');
            sheet1.width(4, 7);
		    sheet1.valign(4, 3, 'center');
		    sheet1.align(4, 3, 'center');
			sheet1.merge({col:4,row:3},{col:4,row:4});
		
		    sheet1.set(5, 3, 'NONE'); 
            sheet1.font(5,3,{bold:true,sz:11}); 
            sheet1.border(5, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(5, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(5, 3, 'true');
            sheet1.width(5, 11);
		    sheet1.valign(5, 3, 'center');
		    sheet1.align(5, 3, 'center');
			sheet1.merge({col:5,row:3},{col:5,row:4});
			
			sheet1.set(6, 3, 'TOTAL'); 
            sheet1.font(6,3,{bold:true,sz:12}); 
            sheet1.border(6, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(6, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(6, 3, 'true');
            sheet1.width(6, 10);
		    //sheet1.merge({col:6,row:2},{col:6,row:3});
		    sheet1.align(6, 3, 'center');
		    sheet1.valign(6, 3, 'center');
			sheet1.merge({col:6,row:3},{col:6,row:4});
			
			
			//3rd row currentdate
			sheet1.set(7, 3, '20+2'); 
            sheet1.font(7,3,{bold:true,sz:12}); 
            sheet1.border(7, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(8, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(9, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(7, 3, 'true');
            sheet1.width(7, 20);
		    sheet1.merge({col:7,row:3},{col:9,row:3});
		    sheet1.align(7, 3, 'center');
		    sheet1.valign(7, 3, 'center');
		    sheet1.fill(7, 3, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		    sheet1.fill(8, 3, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(9, 3, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			
			sheet1.set(10, 3, '10+1'); 
            sheet1.font(10,3,{bold:true,sz:12}); 
            sheet1.border(10, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(11, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(12, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(10, 3, 'true');
            sheet1.width(10, 20);
		    sheet1.merge({col:10,row:3},{col:12,row:3});
		    sheet1.align(10, 3, 'center');
		    sheet1.valign(10, 3, 'center');
		    sheet1.fill(10, 3, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		    sheet1.fill(11, 3, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(12, 3, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			
			sheet1.set(13, 3, '3+1'); 
            sheet1.font(13,3,{bold:true,sz:12}); 
            sheet1.border(13, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(14, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(15, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(13, 3, 'true');
            sheet1.width(13, 20);
		    sheet1.merge({col:13,row:3},{col:15,row:3});
		    sheet1.align(13, 3, 'center');
		    sheet1.valign(13, 3, 'center');
		    sheet1.fill(13, 3, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		    sheet1.fill(14, 3, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(15, 3, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			
			sheet1.set(16, 3, '20+2'); 
            sheet1.font(16,3,{bold:true,sz:11}); 
            sheet1.border(16, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(16, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(16, 3, 'true');
            sheet1.width(16, 11);
		    sheet1.valign(16, 3, 'center');
		    sheet1.align(16, 3, 'center');
		    sheet1.fill(16, 3, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(16, 4, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.merge({col:16,row:3},{col:16,row:4});
			
			sheet1.set(17, 3, '10+1'); 
            sheet1.font(17,3,{bold:true,sz:11}); 
            sheet1.border(17, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(17, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(17, 3, 'true');
            sheet1.width(17, 11);
		    sheet1.valign(17, 3, 'center');
		    sheet1.align(17, 3, 'center');
		    sheet1.fill(17, 3, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(17, 4, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.merge({col:17,row:3},{col:17,row:4});
			
			sheet1.set(18, 3, '3+1'); 
            sheet1.font(18,3,{bold:true,sz:11}); 
            sheet1.border(18, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(18, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(18, 3, 'true');
            sheet1.width(18, 11);
		    sheet1.valign(18, 3, 'center');
		    sheet1.align(18, 3, 'center');
		    sheet1.fill(18, 3, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.fill(18, 4, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			sheet1.merge({col:18,row:3},{col:18,row:4});
			
			//3rd row previousdate
			sheet1.set(19, 3, '20+2'); 
            sheet1.font(19,3,{bold:true,sz:12}); 
            sheet1.border(19, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(20, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(21, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(19, 3, 'true');
            sheet1.width(19, 20);
		    sheet1.merge({col:19,row:3},{col:21,row:3});
		    sheet1.align(19, 3, 'center');
		    sheet1.valign(19, 3, 'center');
		    sheet1.fill(19, 3, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		    sheet1.fill(20, 3, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(21, 3, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			
			sheet1.set(22, 3, '10+1'); 
            sheet1.font(22,3,{bold:true,sz:12}); 
            sheet1.border(22, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(23, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(24, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(22, 3, 'true');
            sheet1.width(22, 20);
		    sheet1.merge({col:22,row:3},{col:24,row:3});
		    sheet1.align(22, 3, 'center');
		    sheet1.valign(22, 3, 'center');
		    sheet1.fill(22, 3, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		    sheet1.fill(23, 3, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(24, 3, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			
			sheet1.set(25, 3, '3+1'); 
            sheet1.font(25,3,{bold:true,sz:12}); 
            sheet1.border(25, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(26, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(27, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(25, 3, 'true');
            sheet1.width(25, 20);
		    sheet1.merge({col:25,row:3},{col:27,row:3});
		    sheet1.align(25, 3, 'center');
		    sheet1.valign(25, 3, 'center');
		    sheet1.fill(25, 3, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		    sheet1.fill(26, 3, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(27, 3, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			
			sheet1.set(28, 3, '20+2'); 
            sheet1.font(28,3,{bold:true,sz:11}); 
            sheet1.border(28, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(28, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(28, 3, 'true');
            sheet1.width(28, 11);
		    sheet1.valign(28, 3, 'center');
		    sheet1.align(28, 3, 'center');
		    sheet1.fill(28, 3, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(28, 4, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.merge({col:28,row:3},{col:28,row:4});
			
			sheet1.set(29, 3, '10+1'); 
            sheet1.font(29,3,{bold:true,sz:11}); 
            sheet1.border(29, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.border(29, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.wrap(29, 3, 'true');
            sheet1.width(29, 11);
		    sheet1.valign(29, 3, 'center');
		    sheet1.align(29, 3, 'center');
		    sheet1.fill(29, 3, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(29, 4, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.merge({col:29,row:3},{col:29,row:4});
			
			sheet1.set(30, 3, '3+1'); 
            sheet1.font(30,3,{bold:true,sz:11}); 
            sheet1.border(30, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(30, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(30, 3, 'true');
            sheet1.width(30, 11);
		    sheet1.valign(30, 3, 'center');
		    sheet1.align(30, 3, 'center');
		    sheet1.fill(30, 3, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.fill(30, 4, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			sheet1.merge({col:30,row:3},{col:30,row:4});
			
			//3rd row total
			sheet1.set(31, 3, '20+2'); 
            sheet1.font(31,3,{bold:true,sz:12}); 
            sheet1.border(31, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(32, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(33, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(31, 3, 'true');
            sheet1.width(31, 20);
		    sheet1.merge({col:31,row:3},{col:33,row:3});
		    sheet1.align(31, 3, 'center');
		    sheet1.valign(31, 3, 'center');
		    sheet1.fill(31, 3, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		    sheet1.fill(32, 3, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(33, 3, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			
			sheet1.set(34, 3, '10+1'); 
            sheet1.font(34,3,{bold:true,sz:12}); 
            sheet1.border(34, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(35, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(36, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(34, 3, 'true');
            sheet1.width(34, 20);
		    sheet1.merge({col:34,row:3},{col:36,row:3});
		    sheet1.align(34, 3, 'center');
		    sheet1.valign(34, 3, 'center');
		    sheet1.fill(34, 3, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		    sheet1.fill(35, 3, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(36, 3, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			
			sheet1.set(37, 3, '3+1'); 
            sheet1.font(37,3,{bold:true,sz:12}); 
            sheet1.border(37, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(38, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(39, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(37, 3, 'true');
            sheet1.width(37, 20);
		    sheet1.merge({col:37,row:3},{col:39,row:3});
		    sheet1.align(37, 3, 'center');
		    sheet1.valign(37, 3, 'center');
		    sheet1.fill(37, 3, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
		    sheet1.fill(38, 3, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(39, 3, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			
			sheet1.set(40, 3, '20+2'); 
            sheet1.font(40,3,{bold:true,sz:11}); 
            sheet1.border(40, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(40, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(40, 3, 'true');
            sheet1.width(40, 11);
		    sheet1.valign(40, 3, 'center');
		    sheet1.align(40, 3, 'center');
		    sheet1.fill(40, 3, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(40, 4, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.merge({col:40,row:3},{col:40,row:4});
			
			sheet1.set(41, 3, '10+1'); 
            sheet1.font(41,3,{bold:true,sz:11}); 
            sheet1.border(41, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(41, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(41, 3, 'true');
            sheet1.width(41, 11);
		    sheet1.valign(41, 3, 'center');
		    sheet1.align(41, 3, 'center');
		    sheet1.fill(41, 3, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(41, 4, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.merge({col:41,row:3},{col:41,row:4});
			
			sheet1.set(42, 3, '3+1'); 
            sheet1.font(42,3,{bold:true,sz:11}); 
            sheet1.border(42, 3, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			sheet1.border(42, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(42, 3, 'true');
            sheet1.width(42, 11);
		    sheet1.valign(42, 3, 'center');
		    sheet1.align(42, 3, 'center');
		    sheet1.fill(42, 3, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.fill(42, 4, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			sheet1.merge({col:42,row:3},{col:42,row:4});
			
			
			
		
		    //1st-29th child headings........
		    sheet1.set(7, 4, 'NEW'); 
            sheet1.font(7,4,{bold:true,sz:11}); 
            sheet1.border(7, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(7, 4, 'true');
            sheet1.width(7, 11);
		    sheet1.valign(7, 4, 'center');
		    sheet1.align(7, 4, 'center');
		    sheet1.fill(7, 4, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		
		    sheet1.set(8, 4, 'REPEAT'); 
            sheet1.font(8,4,{bold:true,sz:11}); 
            sheet1.border(4, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(8, 4, 'true');
            sheet1.width(8, 11);
		    sheet1.valign(8, 4, 'center');
		    sheet1.align(8, 4, 'center');
		    sheet1.fill(8, 4, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		
		    sheet1.set(9, 4, 'TOTAL'); 
            sheet1.font(9,4,{bold:true,sz:11}); 
            sheet1.border(9, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(9, 4, 'true');
            sheet1.width(9, 11);
		    sheet1.valign(9, 4, 'center');
		    sheet1.align(9, 4, 'center');
		    sheet1.fill(9, 4, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		
		    sheet1.set(10, 4, 'NEW'); 
            sheet1.font(10,4,{bold:true,sz:11}); 
            sheet1.border(10, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(10, 4, 'true');
            sheet1.width(10, 17);
		    sheet1.valign(10, 4, 'center');
		    sheet1.align(10, 4, 'center');
		    sheet1.fill(10, 4, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		
		    sheet1.set(11, 4, 'REPEAT'); 
            sheet1.font(11,4,{bold:true,sz:11}); 
            sheet1.border(11, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(11, 4, 'true');
            sheet1.width(11, 17);
		    sheet1.valign(11, 4, 'center');
		    sheet1.align(11, 4, 'center');
		    sheet1.fill(11, 4, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		
		    sheet1.set(12, 4, 'TOTAL'); 
            sheet1.font(12,4,{bold:true,sz:11}); 
            sheet1.border(12, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(12, 4, 'true');
            sheet1.width(12, 17);
		    sheet1.valign(12, 4, 'center');
		    sheet1.align(12, 4, 'center');
		    sheet1.fill(12, 4, {type:'solid',fgColor:'99FF99',bgColor:'64'});
			
			sheet1.set(13, 4, 'NEW'); 
            sheet1.font(13,4,{bold:true,sz:11}); 
            sheet1.border(13, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(13, 4, 'true');
            sheet1.width(13, 17);
		    sheet1.valign(13, 4, 'center');
		    sheet1.align(13, 4, 'center');
		    sheet1.fill(13, 4, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		
		    //current data child headings...........
		    sheet1.set(14, 4, 'REPEAT'); 
            sheet1.font(14,4,{bold:true,sz:11}); 
            sheet1.border(14, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(14, 4, 'true');
            sheet1.width(14, 11);
		    sheet1.valign(14, 4, 'center');
		    sheet1.align(14, 4, 'center');
		    sheet1.fill(14, 4, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		
		    sheet1.set(15, 4, 'TOTAL'); 
            sheet1.font(15,4,{bold:true,sz:11}); 
            sheet1.border(15, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(15, 4, 'true');
            sheet1.width(15, 11);
		    sheet1.valign(15, 4, 'center');
		    sheet1.align(15, 4, 'center');
		    sheet1.fill(15, 4, {type:'solid',fgColor:'99FF99',bgColor:'64'});
		
		    
		
		    sheet1.set(19, 4, 'NEW'); 
            sheet1.font(19,4,{bold:true,sz:11}); 
            sheet1.border(19, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(19, 4, 'true');
            sheet1.width(19, 17);
		    sheet1.valign(19, 4, 'center');
		    sheet1.align(19, 4, 'center');
		    sheet1.fill(19, 4, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			
			sheet1.set(20, 4, 'REPEAT'); 
            sheet1.font(20,4,{bold:true,sz:11}); 
            sheet1.border(20, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(20, 4, 'true');
            sheet1.width(20, 17);
		    sheet1.valign(20, 4, 'center');
		    sheet1.align(20, 4, 'center');
		    sheet1.fill(20, 4, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		
		    //total data child headings.............
		    sheet1.set(21, 4, 'TOTAL'); 
            sheet1.font(21,4,{bold:true,sz:11}); 
            sheet1.border(21, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(21, 4, 'true');
            sheet1.width(21, 11);
		    sheet1.valign(21, 4, 'center');
		    sheet1.align(21, 4, 'center');
		    sheet1.fill(21, 4, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		
		    sheet1.set(22, 4, 'NEW'); 
            sheet1.font(22,4,{bold:true,sz:11}); 
            sheet1.border(22, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(22, 4, 'true');
            sheet1.width(22, 11);
		    sheet1.valign(22, 4, 'center');
		    sheet1.align(22, 4, 'center');
		    sheet1.fill(22, 4, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		
		    sheet1.set(23, 4, 'REPEAT'); 
            sheet1.font(23,4,{bold:true,sz:11}); 
            sheet1.border(23, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(23, 4, 'true');
            sheet1.width(23, 11);
		    sheet1.valign(23, 4, 'center');
		    sheet1.align(23, 4, 'center');
		    sheet1.fill(23, 4, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		
		    sheet1.set(24, 4, 'TOTAL'); 
            sheet1.font(24,4,{bold:true,sz:11}); 
            sheet1.border(24, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(24, 4, 'true');
            sheet1.width(24, 17);
		    sheet1.valign(24, 4, 'center');
		    sheet1.align(24, 4, 'center');
		    sheet1.fill(24, 4, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		
		    sheet1.set(25, 4, 'NEW'); 
            sheet1.font(25,4,{bold:true,sz:11}); 
            sheet1.border(25, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(25, 4, 'true');
            sheet1.width(25, 17);
		    sheet1.valign(25, 4, 'center');
		    sheet1.align(25, 4, 'center');
		    sheet1.fill(25, 4, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
		
		    sheet1.set(26, 4, 'REPEAT'); 
            sheet1.font(26,4,{bold:true,sz:11}); 
            sheet1.border(26, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(26, 4, 'true');
            sheet1.width(26, 17);
		    sheet1.valign(26, 4, 'center');
		    sheet1.align(26, 4, 'center');
		    sheet1.fill(26, 4, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			
			sheet1.set(27, 4, 'TOTAL'); 
            sheet1.font(27,4,{bold:true,sz:11}); 
            sheet1.border(27, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(27, 4, 'true');
            sheet1.width(27, 17);
		    sheet1.valign(27, 4, 'center');
		    sheet1.align(27, 4, 'center');
		    sheet1.fill(27, 4, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
			
			sheet1.set(31, 4, 'NEW'); 
            sheet1.font(31,4,{bold:true,sz:11}); 
            sheet1.border(31, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(31, 4, 'true');
            sheet1.width(31, 17);
		    sheet1.valign(31, 4, 'center');
		    sheet1.align(31, 4, 'center');
		    sheet1.fill(31, 4, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			
			sheet1.set(32, 4, 'REPEAT'); 
            sheet1.font(32,4,{bold:true,sz:11}); 
            sheet1.border(32, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(32, 4, 'true');
            sheet1.width(32, 17);
		    sheet1.valign(32, 4, 'center');
		    sheet1.align(32, 4, 'center');
		    sheet1.fill(32, 4, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			
			sheet1.set(33, 4, 'TOTAL'); 
            sheet1.font(33,4,{bold:true,sz:11}); 
            sheet1.border(33, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(33, 4, 'true');
            sheet1.width(33, 17);
		    sheet1.valign(33, 4, 'center');
		    sheet1.align(33, 4, 'center');
		    sheet1.fill(33, 4, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			
			sheet1.set(34, 4, 'NEW'); 
            sheet1.font(34,4,{bold:true,sz:11}); 
            sheet1.border(34, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(34, 4, 'true');
            sheet1.width(34, 17);
		    sheet1.valign(34, 4, 'center');
		    sheet1.align(34, 4, 'center');
		    sheet1.fill(34, 4, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			
			sheet1.set(35, 4, 'REPEAT'); 
            sheet1.font(35,4,{bold:true,sz:11}); 
            sheet1.border(35, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(35, 4, 'true');
            sheet1.width(35, 17);
		    sheet1.valign(35, 4, 'center');
		    sheet1.align(35, 4, 'center');
		    sheet1.fill(35, 4, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			
			sheet1.set(36, 4, 'TOTAL'); 
            sheet1.font(36,4,{bold:true,sz:11}); 
            sheet1.border(36, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(36, 4, 'true');
            sheet1.width(36, 17);
		    sheet1.valign(36, 4, 'center');
		    sheet1.align(36, 4, 'center');
		    sheet1.fill(36, 4, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			
			sheet1.set(37, 4, 'NEW'); 
            sheet1.font(37,4,{bold:true,sz:11}); 
            sheet1.border(37, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(37, 4, 'true');
            sheet1.width(37, 17);
		    sheet1.valign(37, 4, 'center');
		    sheet1.align(37, 4, 'center');
		    sheet1.fill(37, 4, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			
			sheet1.set(38, 4, 'REPEAT'); 
            sheet1.font(38,4,{bold:true,sz:11}); 
            sheet1.border(38, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(38, 4, 'true');
            sheet1.width(38, 17);
		    sheet1.valign(38, 4, 'center');
		    sheet1.align(38, 4, 'center');
		    sheet1.fill(38, 4, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			
			sheet1.set(39, 4, 'TOTAL'); 
            sheet1.font(39,4,{bold:true,sz:11}); 
            sheet1.border(39, 4, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(39, 4, 'true');
            sheet1.width(39, 17);
		    sheet1.valign(39, 4, 'center');
		    sheet1.align(39, 4, 'center');
		    sheet1.fill(39, 4, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
			
			
			
		    
			var j=5;
			for(var skey in hqData){
				
				var pointName = skey;
				
				var A = hqData[skey]['NO OF OUTLETS'].grading.A;
				var B = hqData[skey]['NO OF OUTLETS'].grading.B;
				var C = hqData[skey]['NO OF OUTLETS'].grading.C;
				var none = hqData[skey]['NO OF OUTLETS'].grading.NONE;
				var outltsTotal = hqData[skey]['NO OF OUTLETS'].total;
				
				var prevNEW_1001 = hqData[skey].Previous.sale.new['1001'];
				var prevNEW_2002 = hqData[skey].Previous.sale.new['2002'];
				var prevNEW_0301 = hqData[skey].Previous.sale.new['0301'];
				var prevTotal_1001 = hqData[skey].Previous.sale.total['1001'];
				var prevTotal_2002 = hqData[skey].Previous.sale.total['2002'];
				var prevTotal_0301 = hqData[skey].Previous.sale.total['0301'];
				var prevStock_1001 = hqData[skey].Previous.stock['1001'];
				var prevStock_2002 = hqData[skey].Previous.stock['2002'];
				var prevStock_0301 = hqData[skey].Previous.stock['0301'];
				
				var currNEW_1001 = hqData[skey].Today.sale.new['1001'];
				var currNEW_2002 = hqData[skey].Today.sale.new['2002'];
				var currNEW_0301 = hqData[skey].Today.sale.new['0301'];
				var currTotal_1001 = hqData[skey].Today.sale.total['1001'];
				var currTotal_2002 = hqData[skey].Today.sale.total['2002'];
				var currTotal_0301 = hqData[skey].Today.sale.total['0301'];
				var currStock_1001 = hqData[skey].Today.stock['1001'];
				var currStock_2002 = hqData[skey].Today.stock['2002'];
				var currStock_0301 = hqData[skey].Today.stock['0301'];
				
				var prevREPEAT_1001 = Number(prevTotal_1001)-Number(prevNEW_1001);
				var prevREPEAT_2002 = Number(prevTotal_2002)-Number(prevNEW_2002);
				var prevREPEAT_0301 = Number(prevTotal_0301)-Number(prevNEW_0301);
				var currREPEAT_1001 = Number(currTotal_1001)-Number(currNEW_1001);
				var currREPEAT_2002 = Number(currTotal_2002)-Number(currNEW_2002);
				var currREPEAT_0301 = Number(currTotal_0301)-Number(currNEW_1001);
				
				var totNew_1001 = Number(prevNEW_1001)+Number(currNEW_1001);
				var totNew_2002 = Number(prevNEW_2002)+Number(currNEW_2002);
				var totNew_0301 = Number(prevNEW_0301)+Number(currNEW_0301);
				
				var totRepeat_1001 = Number(prevREPEAT_1001)+Number(currREPEAT_1001);
				var totRepeat_2002 = Number(prevREPEAT_2002)+Number(currREPEAT_2002);
				var totRepeat_0301 = Number(prevREPEAT_0301)+Number(currREPEAT_0301);
				
				var grandTotal_1001 = Number(prevTotal_1001)+Number(currTotal_1001);
				var grandTotal_2002 = Number(prevTotal_2002)+Number(currTotal_2002);
				var grandTotal_0301 = Number(prevTotal_0301)+Number(currTotal_0301);
				
				//var totStock = Number(prevStock)+Number(currStock);
				
				
				sheet1.set(1, j, pointName); 
	            sheet1.border(1, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(1, j, 'center');
				
				sheet1.set(2, j, A); 
	            sheet1.border(2, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(2, j, 'center');
				
				sheet1.set(3, j, B); 
	            sheet1.border(3, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(3, j, 'center');
				
				sheet1.set(4, j, C); 
	            sheet1.border(4, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(4, j, 'center');
				
				sheet1.set(5, j, none); 
	            sheet1.border(5, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(5, j, 'center');
				
				sheet1.set(6, j, outltsTotal); 
	            sheet1.border(6, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(6, j, 'center');
				
				sheet1.set(7, j, Math.abs(currNEW_2002)); 
	            sheet1.border(7, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(7, j, 'center');
				sheet1.fill(7, j, {type:'solid',fgColor:'99FF99',bgColor:'64'});
				
				sheet1.set(8, j, Math.abs(currREPEAT_2002)); 
	            sheet1.border(8, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(8, j, 'center');
				sheet1.fill(8, j, {type:'solid',fgColor:'99FF99',bgColor:'64'});
				
				sheet1.set(9, j, Math.abs(currTotal_2002)); 
	            sheet1.border(9, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(9, j, 'center');
				sheet1.fill(9, j, {type:'solid',fgColor:'99FF99',bgColor:'64'});
				
				sheet1.set(10, j, Math.abs(currNEW_1001)); 
	            sheet1.border(10, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(10, j, 'center');
				sheet1.fill(10, j, {type:'solid',fgColor:'99FF99',bgColor:'64'});
				
				sheet1.set(11, j, Math.abs(currREPEAT_1001)); 
	            sheet1.border(11, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(11, j, 'center');
				sheet1.fill(11, j, {type:'solid',fgColor:'99FF99',bgColor:'64'});
				
				sheet1.set(12, j, currTotal_1001); 
	            sheet1.border(12, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(12, j, 'center');
				sheet1.fill(12, j, {type:'solid',fgColor:'99FF99',bgColor:'64'});
				
				sheet1.set(13, j, currNEW_0301); 
	            sheet1.border(13, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(13, j, 'center');
				sheet1.fill(13, j, {type:'solid',fgColor:'99FF99',bgColor:'64'});
				
				sheet1.set(14, j, Math.abs(currREPEAT_0301)); 
	            sheet1.border(14, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(14, j, 'center');
				sheet1.fill(14, j, {type:'solid',fgColor:'99FF99',bgColor:'64'});
				
				sheet1.set(15, j, Math.abs(currTotal_0301)); 
	            sheet1.border(15, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(15, j, 'center');
				sheet1.fill(15, j, {type:'solid',fgColor:'99FF99',bgColor:'64'});
				
				sheet1.set(16, j, Math.abs(currStock_2002)); 
	            sheet1.border(16, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(16, j, 'center');
				sheet1.fill(16, j, {type:'solid',fgColor:'99FF99',bgColor:'64'});
				
				sheet1.set(17, j, Math.abs(currStock_1001)); 
	            sheet1.border(17, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(17, j, 'center');
				sheet1.fill(17, j, {type:'solid',fgColor:'99FF99',bgColor:'64'});
				
				sheet1.set(18, j, currStock_0301); 
	            sheet1.border(18, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(18, j, 'center');
				sheet1.fill(18, j, {type:'solid',fgColor:'99FF99',bgColor:'64'});
				
				sheet1.set(19, j, prevNEW_2002); 
	            sheet1.border(19, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(19, j, 'center');
				sheet1.fill(19, j, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
				
				sheet1.set(20, j, prevREPEAT_2002); 
	            sheet1.border(20, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(20, j, 'center');
				sheet1.fill(20, j, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
				
				sheet1.set(21, j, Math.abs(prevTotal_2002)); 
	            sheet1.border(21, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(21, j, 'center');
				sheet1.fill(21, j, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
				
				sheet1.set(22, j, Math.abs(prevNEW_1001)); 
	            sheet1.border(22, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(22, j, 'center');
				sheet1.fill(22, j, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
				
				sheet1.set(23, j, Math.abs(prevREPEAT_1001)); 
	            sheet1.border(23, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(23, j, 'center');
				sheet1.fill(23, j, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
				
				sheet1.set(24, j, prevTotal_1001); 
	            sheet1.border(24, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(24, j, 'center');
				sheet1.fill(24, j, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
				
				sheet1.set(25, j, prevNEW_0301); 
	            sheet1.border(25, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(25, j, 'center');
				sheet1.fill(25, j, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
				
				sheet1.set(26, j, prevREPEAT_0301); 
	            sheet1.border(26, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(26, j, 'center');
				sheet1.fill(26, j, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
				
				sheet1.set(27, j, prevTotal_0301); 
	            sheet1.border(27, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(27, j, 'center');
				sheet1.fill(27, j, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
				
				sheet1.set(28, j, prevStock_2002); 
	            sheet1.border(28, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(28, j, 'center');
				sheet1.fill(28, j, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
				
				sheet1.set(29, j, prevStock_1001); 
	            sheet1.border(29, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(29, j, 'center');
				sheet1.fill(29, j, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
				
				sheet1.set(30, j, prevStock_0301); 
	            sheet1.border(30, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(30, j, 'center');
				sheet1.fill(30, j, {type:'solid',fgColor:'E0E0E0',bgColor:'64'});
				
				sheet1.set(31, j, totNew_2002); 
	            sheet1.border(31, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(31, j, 'center');
				sheet1.fill(31, j, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
				
				sheet1.set(32, j, totRepeat_2002); 
	            sheet1.border(32, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(32, j, 'center');
				sheet1.fill(32, j, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
				
				sheet1.set(33, j, grandTotal_2002); 
	            sheet1.border(33, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(33, j, 'center');
				sheet1.fill(33, j, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
				
				sheet1.set(34, j, totNew_1001); 
	            sheet1.border(34, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(34, j, 'center');
				sheet1.fill(34, j, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
				
				sheet1.set(35, j, totRepeat_1001); 
	            sheet1.border(35, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(35, j, 'center');
				sheet1.fill(35, j, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
				
				sheet1.set(36, j, grandTotal_1001); 
	            sheet1.border(36, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(36, j, 'center');
				sheet1.fill(36, j, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
				
				sheet1.set(37, j, totNew_0301); 
	            sheet1.border(37, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(37, j, 'center');
				sheet1.fill(37, j, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
				
				sheet1.set(38, j, totRepeat_0301); 
	            sheet1.border(38, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(38, j, 'center');
				sheet1.fill(38, j, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
				
				sheet1.set(39, j, grandTotal_1001); 
	            sheet1.border(39, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(39, j, 'center');
				sheet1.fill(39, j, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
				
				sheet1.set(40, j, currStock_2002); 
	            sheet1.border(40, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(40, j, 'center');
				sheet1.fill(40, j, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
				
				sheet1.set(41, j, currStock_1001); 
	            sheet1.border(41, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(41, j, 'center');
				sheet1.fill(41, j, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
				
				sheet1.set(42, j, currStock_0301); 
	            sheet1.border(42, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
			    sheet1.valign(42, j, 'center');
				sheet1.fill(42, j, {type:'solid',fgColor:'FFFF66',bgColor:'64'});
				
				j++;
				
			}
			
		}
        // Save it 
        workbook.save(function(ok){
            var file = 'Sales-Report-'+lastdayPostFix+'-'+lmonth+'.xlsx';
			fs.readFile('./Reports/Sales/' +file, function (err, content) {
			    if(err) throw err;
                res.setHeader('Content-disposition', 'attachment; filename='+file);
                res.end(content);
            
            });
		    fs.unlink('./Reports/Sales/' +file); //deleting file after downloaded*/
        });
            
       });
        
	});
}

function downloadActivitySheet(req,res){
    var timestamp = Number(new Date());
    var dateobj= new Date() ;
    var day = dateobj.getDate() ;
    var year = dateobj.getFullYear();
	var lastDay = new Date((dateobj.setDate(dateobj.getDate()-1)));
    var lastdayPostFix = dayPostFix(lastDay.getDate());
    var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var monthname = monthNames[dateobj.getMonth()];
	var lmonth = monthNames[lastDay.getMonth()];
    mkpath('./Reports/Activity', function (err){
        if (err) throw err;
   	    
        var workbook = excelbuilder.createWorkbook('./Reports/Activity/', 'Activity-Report-'+lastdayPostFix+'-'+lmonth+'.xlsx');
		
		var options = { method: 'GET',
            url: 'http://192.168.99.76/reports/cer/cerFEReport.php',  //http://192.168.99.76/reports/cer/
            headers: 
            { 'postman-token': '18cac29b-e66e-a8c9-407d-bfdbc0af0eb9',
              'cache-control': 'no-cache' } };

            request(options, function (error, response, body) {
				var data = JSON.parse(body);
        for(var key in data[0]){
		    var hqData = data[0][key];
		    var length = Object.keys(data[0][key]).length;
		    if(key=='HYD'){ var hq ='Hyderabad'; } 
		    else if(key=='BOM'){ var hq = 'Mumbai'; } 
		    else if(key=='CCU'){ var hq = 'Kolkata'; } 
		    else if(key=='PNQ') { var hq = 'Pune'; } 
		    else if(key=='BLR') { var hq = 'Bangalore'; }  
		    else if(key=='DEL') { var hq = 'Delhi'; }  
            var sheet1 = workbook.createSheet(hq, 100, 1200);
        
            sheet1.set(1, 1, 'NAME'); 
            sheet1.font(1,1,{bold:true,sz:12}); 
            sheet1.border(1, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(1, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(1, 1, 'true');
            sheet1.width(1, 20);
		    sheet1.merge({col:1,row:1},{col:1,row:2});
			sheet1.align(1, 1, 'center');
		    sheet1.valign(1, 1, 'center');

            sheet1.set(2, 1, 'ROLE'); 
            sheet1.font(2,1,{bold:true,sz:12});  
            sheet1.border(2, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(2, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(2, 1, 'true');
            sheet1.width(2, 20);
		    sheet1.merge({col:2,row:1},{col:2,row:2});
			sheet1.align(2, 1, 'center');
		    sheet1.valign(2, 1, 'center');

            sheet1.set(3, 1, 'PHONE NUMBER'); 
            sheet1.font(3,1,{bold:true,sz:12}); 
            sheet1.border(3, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(3, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(3, 1, 'true');
            sheet1.width(3, 20);
		    sheet1.merge({col:3,row:1},{col:3,row:2});
			sheet1.align(3, 1, 'center');
		    sheet1.valign(3, 1, 'center');

            sheet1.set(4, 1, 'ATTENDANCE IN'); 
            sheet1.font(4,1,{bold:true,sz:12}); 
            sheet1.border(4, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(4, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(4, 1, 'true');
            sheet1.width(4, 20);
		    sheet1.merge({col:4,row:1},{col:4,row:2});
			sheet1.align(4, 1, 'center');
		    sheet1.valign(4, 1, 'center');

            sheet1.set(5, 1, 'ATTENDANCE OUT'); 
            sheet1.font(5,1,{bold:true,sz:12}); 
            sheet1.border(5, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(5, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(5, 1, 'true');
            sheet1.width(5, 20);
		    sheet1.merge({col:5,row:1},{col:5,row:2});
			sheet1.align(5, 1, 'center');
		    sheet1.valign(5, 1, 'center');

            sheet1.set(6, 1, 'TIME DIFFERENCE(HH:MM)'); 
            sheet1.font(6,1,{bold:true,sz:12}); 
            sheet1.border(6, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(6, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(6, 1, 'true');
            sheet1.width(6, 22);
		    sheet1.merge({col:6,row:1},{col:6,row:2});
			sheet1.align(6, 1, 'center');
		    sheet1.valign(6, 1, 'center');

            sheet1.set(7, 1, 'NUMBER OF STOCK UPDATES'); 
            sheet1.font(7,1,{bold:true,sz:12}); 
            sheet1.border(7, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(7, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(7, 1, 'true');
            sheet1.width(7, 20);
		    sheet1.merge({col:7,row:1},{col:7,row:2});
			sheet1.align(7, 1, 'center');
		    sheet1.valign(7, 1, 'center');

            sheet1.set(8, 1, 'TOTAL SALE');
            sheet1.font(8,1,{bold:true,sz:12}); 
            sheet1.border(8, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(8, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(8, 1, 'true');
            sheet1.width(8, 22);
		    sheet1.merge({col:8,row:1},{col:8,row:2});
			sheet1.align(8, 1, 'center');
		    sheet1.valign(8, 1, 'center');

            sheet1.set(9, 1, 'DRP'); 
            sheet1.font(9,1,{bold:true,sz:12}); 
            sheet1.border(9,1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(10, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.border(11, 1, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		    sheet1.wrap(9, 1, 'true');
            sheet1.width(9, 20);
		    sheet1.merge({col:9,row:1},{col:11,row:1});
		    sheet1.align(9, 1, 'center');
		
            
		
		    //DRP sub headings..........
		    sheet1.set(9, 2, 'ASSIGNING');
            sheet1.font(9,2,{bold:true,sz:10}); 
            sheet1.border(9, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(9, 2, 'true');
            sheet1.width(9, 22);
			sheet1.align(9, 2, 'center');
		    sheet1.valign(9, 2, 'center');
		
		    sheet1.set(10, 2, 'PENDING');
            sheet1.font(10,2,{bold:true,sz:10}); 
            sheet1.border(10, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(10, 2, 'true');
            sheet1.width(10, 22);
			sheet1.align(10, 2, 'center');
		    sheet1.valign(10, 2, 'center');
		
		    sheet1.set(11, 2, 'COMPLETED');
            sheet1.font(11,2,{bold:true,sz:10}); 
            sheet1.border(11, 2, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
            sheet1.wrap(11, 2, 'true');
            sheet1.width(11, 22);
			sheet1.align(11, 2, 'center');
		    sheet1.valign(11, 2, 'center');
		
		    var k=3;
		    for(var skey in hqData){
				
				var name = hqData[skey]['Name'];
				var roleCode = hqData[skey]['roleCode'];
				var phone = hqData[skey]['Phone Number'];
				var stock = hqData[skey]['number of stock'];
				var sale = hqData[skey]['number of sale'];
				var tsale = hqData[skey]['total sale'];
				var promotions = hqData[skey]['promotions'];
				var drp = hqData[skey]['DRP'];
				if(promotions==null){
				    var videoPromotions = "";
				}
				else{
				    var videoPromotions = promotions.hasOwnProperty('video promotion')?hqData[skey]['promotions']['video promotion']:'';
				}
				if(drp==null){
					var complete = "";
					var pending = "";
					var assigning = "";
				}
				else{
				    var complete = drp.hasOwnProperty('Completed')? hqData[skey]['DRP']['Completed']:'';
				    var pending = drp.hasOwnProperty('Pending')? hqData[skey]['DRP']['Pending']:'';
					var assigning = Number(complete)+Number(pending);
				}
				   
				var attn = hqData[skey]['attendance'];
				if(attn==null){
				    var attnLength = 0;
			    }
				else{
					var attnLength = Object.keys(attn).length;
				}
				var j=k;
				if(attnLength>0){
				    var m = (k + attnLength)-1;
                    sheet1.merge({col:1,row:k},{col:1,row:m});
				    sheet1.merge({col:2,row:k},{col:2,row:m});
				    sheet1.merge({col:3,row:k},{col:3,row:m});
				}
				if(attn!=null){   
				for(var ckey in attn){
					if(attnLength>0 &&  attn[ckey]['ASa_startedAt']!=null && attn[ckey]['ASa_endedAt']!=null){
					    var attnIn = attn[ckey]['ASa_startedAt'];
					    var attnOut = attn[ckey]['ASa_endedAt'];
						var login = moment(attnIn).add(5.30,'hours');
					    var logout = moment(attnOut).add(5.30,'hours');
						var logedin = moment(login).format('YYYY-MM-DD HH:mm:ss');
					    var logedof = moment(logout).format('YYYY-MM-DD HH:mm:ss');
					    var hours = timediff(login,logout);
					    var totalHours = hours.hours+":"+hours.minutes;
					   //console.log(totalHours);
					}
					else {
						var attnIn = attn[ckey]['ASa_startedAt'];
					    var attnOut = attn[ckey]['ASa_endedAt'];
						if(attnIn!=null){
						    var login = moment(attnIn).add(5.30,'hours');
						    var logedin = moment(login).format('YYYY-MM-DD HH:mm:ss');
						}
						else{
							var logedin = 'NA';
						}
						if(attnOut!=null){
					       var logout = moment(attnOut).add(5.30,'hours');
						   var logedof = moment(logout).format('YYYY-MM-DD HH:mm:ss');
						}
						else{
							var logedof = 'NA';
						}
						var totalHours = ''; 
					}
					   				
				    sheet1.set(1, j, name.toUpperCase()); 
	                sheet1.border(1, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
					sheet1.valign(1, j, 'center');
					sheet1.wrap(1, j, 'true');
	
	                sheet1.set(2, j, roleCode); 
	                sheet1.border(2, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
					sheet1.valign(2, j, 'center');
	
	                sheet1.set(3, j, phone); 
	                sheet1.border(3, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
					sheet1.valign(3, j, 'center');
	
	                sheet1.set(4, j, logedin); 
	                sheet1.border(4, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
	
	                sheet1.set(5, j, logedof); 
	                sheet1.border(5, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
	
	                sheet1.set(6, j, totalHours); 
	                sheet1.border(6, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
	
	                sheet1.set(7, j, stock); 
	                sheet1.border(7, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
	
	                sheet1.set(8, j, tsale); 
	                sheet1.border(8, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
    
	                sheet1.set(9, j, assigning); 
	                sheet1.border(9, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		
		            sheet1.set(10, j, pending); 
	                sheet1.border(10, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
					
					sheet1.set(11, j, complete); 
	                sheet1.border(11, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
					
									   
					j++;
				}
				   k=j;
				}
				else{
					
					sheet1.set(1, j, name.toUpperCase()); 
	                sheet1.border(1, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
					sheet1.valign(1, j, 'center');
					sheet1.wrap(1, j, 'true');
	
	                sheet1.set(2, j, roleCode); 
	                sheet1.border(2, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
					sheet1.valign(2, j, 'center');
	
	                sheet1.set(3, j, phone); 
	                sheet1.border(3, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
					sheet1.valign(3, j, 'center');
	
	                sheet1.set(4, j, 'NA'); 
	                sheet1.border(4, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
	
	                sheet1.set(5, j, 'NA'); 
	                sheet1.border(5, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
	
	                sheet1.set(6, j, 'NA'); 
	                sheet1.border(6, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
	
	                sheet1.set(7, j, stock); 
	                sheet1.border(7, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
	
	                sheet1.set(8, j, tsale); 
	                sheet1.border(8, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
    
	                sheet1.set(9, j, assigning); 
	                sheet1.border(9, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
		
		            sheet1.set(10, j, pending); 
	                sheet1.border(10, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
					
					sheet1.set(11, j, complete); 
	                sheet1.border(11, j, {left:'medium',top:'medium',right:'medium',bottom:'medium'});
					
					
					j++;
				}
				k=j;
			}
	    }
        
        // Save it 
        workbook.save(function(ok){
            var file = 'Activity-Report-'+lastdayPostFix+'-'+lmonth+'.xlsx';
		    fs.readFile('./Reports/Activity/'+file, function (err, content) {
				if(err) throw err;
                res.setHeader('Content-disposition', 'attachment; filename='+file);
                res.end(content);
            });
		    fs.unlink('./Reports/Activity/'+file); //deleting file after downloaded
        });
            
            

        });
	});
}


function dayPostFix(day){
  if(day==1 || day==31 || day==21 ){
   	 return day+"st";
  }
  else if(day==2 || day==22){
   	  return day+"nd";
  }
  else if(day==3 || day==23){
   	 return day+"rd";
  }
  else{
   	 return day+"th";
  }

}

